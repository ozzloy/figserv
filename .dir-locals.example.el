;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((clojure-mode
  (clojure-indent-style . :always-indent)
  (cider-preferred-build-tool . clojure-cli)
  (cider-default-cljs-repl . figwheel-main)
  (cider-figwheel-main-default-options . "dev")
  (clojure-align-forms-automatically . 1))
 (clojurec-mode
  (cider-default-cljs-repl . figwheel-main))
 (clojurescript-mode
  (cider-default-cljs-repl . figwheel-main)))
