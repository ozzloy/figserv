(ns fyi.tuwo.figserv.client
  (:require-macros
   [cljs.core.async.macros :as asyncm :refer [go go-loop]])
  (:require
   [cljs.core.async :as async :refer [<! >! put! chan]]
   [taoensso.sente :as sente :refer [cb-success?]]
   [reagent.dom :refer [render]]))

(js/console.log "check js console to see this message!")
(js/console.log "this is from src/fyi/tuwo/figserv/client.cljs")

(def ?csrf-token
  (when-let [el (.getElementById js/document "sente-csrf-token")]
    (.getAttribute el "data-csrf-token")))

(let [{:keys [chsk ch-recv send-fn state]}
      (sente/make-channel-socket-client!
        "/chsk" ; Note the same path as before
        ?csrf-token
        {:type :auto ; e/o #{:auto :ajax :ws}
         })]

  (def chsk       chsk)
  (def ch-chsk    ch-recv) ; ChannelSocket's receive channel
  (def chsk-send! send-fn) ; ChannelSocket's send API fn
  (def chsk-state state)   ; Watchable, read-only atom
  )

(defn state-section [state]
  [:div
   [:h1 "state"]
   [:div (str @state)]])

(defn hi-there-section [state]
  [:div
   [:h1 "hi there"]
   [:p "from src/fyi/tuwo/figserv/client.cljs"]])

(defn page [state]
  [:div
   [hi-there-section state]
   [state-section state]])

(defonce web-ui-state (atom nil))

(defn mount [state]
  (render
    [page state]
    (. js/document (getElementById "app"))))

(defn ^:after-load re-render []
  (mount web-ui-state))

(defonce start-up
  (fn []
    (mount web-ui-state)
    true))
