(ns fyi.tuwo.figserv.server
  (:require
   [org.httpkit.server :refer [run-server]]
   [taoensso.sente :as sente]
   [compojure.core :refer :all]
   [compojure.route :as route]
   [ring.middleware.anti-forgery :refer [wrap-anti-forgery] :as rmaf]
   [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
   [ring.middleware.session :refer [wrap-session]]
   [taoensso.sente.server-adapters.http-kit :refer [get-sch-adapter]]
   [hiccup.page :refer [html5 include-css include-js]]))

(let [{:keys [ch-recv send-fn connected-uids
              ajax-post-fn ajax-get-or-ws-handshake-fn]}
      (sente/make-channel-socket! (get-sch-adapter))]
  (def ring-ajax-post ajax-post-fn)
  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)
  (def ch-chsk ch-recv)
  (def chsk-send! send-fn)
  (def connected-uids connected-uids))

(defn home [request]
  (html5 {:lang "en"}
    [:head
     [:meta {:charset "UTF-8"}]
     [:meta {:name    "viewport"
             :content "width=device-width, initial-scale=1"}]
     [:link
      {:rel  "icon"
       :href "https://clojurescript.org/images/cljs-logo-icon-32.png"}]
     [:title "page title here"]]
    [:body
     [:div#app]
     (let [csrf-token (force rmaf/*anti-forgery-token*)]
       [:div#sente-csrf-token {:data-csrf-token csrf-token}])
     (include-js "/cljs-out/dev-main.js")]))

(defroutes figserv-routes
  (GET  "/"     request (home                          request))
  (GET  "/chsk" request (ring-ajax-get-or-ws-handshake request))
  (POST "/chsk" request (ring-ajax-post                request))
  (route/resources "/")
  (route/not-found "<h1>page not found</h1>"))

(def app
  (-> figserv-routes
    (wrap-defaults site-defaults)
    wrap-anti-forgery
    wrap-session))

(defonce server (atom nil))
(defn stop-server! []
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)))

(defn start-server! []
  (stop-server!)
  (let [stop-fn (run-server #'app {:port 4000})]
    (reset! server stop-fn)))

(comment
  )
